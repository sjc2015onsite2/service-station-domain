package com.expositds.sjc.servicestation.domain.model;

import java.util.HashSet;

/**
 * Класс содержит информацию об отзывах лиентов о механиках.
 * 
 * @author Alexey Suslov
 */
public class MechanicPofile {
	
	/**
	 * Идентификатор профиля механика.
	 */
	private Long mechanicPofileId;
	
	/**
	 * Список комментариев о механике.
	 */
	private HashSet<Comment> comments = new HashSet<>();

	public HashSet<Comment> getComments() {
		return comments;
	}

	public void setComments(HashSet<Comment> comments) {
		this.comments = comments;
	}

	public Long getMechanicPofileId() {
		return mechanicPofileId;
	}

	public void setMechanicPofileId(Long mechanicPofileId) {
		this.mechanicPofileId = mechanicPofileId;
	}
		
}
