package com.expositds.sjc.servicestation.domain.model;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Класс Affilate содержит иформацию о Person, Part, Service, PartOrder, Order.
 * 
 * @author Alexey Suslov
 */
public class Affilate {
	
	/**
	 * Идентификатор филиала.
	 */
	private Long affilateId; 
	
	/**
	 * Список запасных частей в филиале с указанием их количества.
	 */
	private HashMap<Part, Integer> parts = new HashMap<>();

	/**
	 * Список заявок у филиала.
	 */
	private HashSet<Order> orders = new HashSet<>();

	/**
	 * Список заявок на запчасти с указанием механика, который создал завку на запчасть.
	 */
	private HashMap<PartOrder, Person> partOrders = new HashMap<>();

	/**
	 * Список сотрудников (механиков) работающих в филиале.
	 */
	private HashSet<Person> persons = new HashSet<>();

	/**
	 * Список оказываемых филиалом услуг и их стоимость.
	 */
	private HashMap<Service, Integer> services = new HashMap<>();

	/**
	 * Код филиала.
	 */
	private String affilateCode;
	
	/**
	 * Создаёт филлиал.
	 * 
	 * @param affilateCode униальный в рамках СТО код филиала
	 */
	public Affilate(String affilateCode) {
		this.affilateCode = affilateCode;
	}

	public HashMap<Part, Integer> getParts() {
		return parts;
	}

	public void setParts(HashMap<Part, Integer> parts) {
		this.parts = parts;
	}

	public HashSet<Order> getOrders() {
		return orders;
	}

	public void setOrders(HashSet<Order> orders) {
		this.orders = orders;
	}

	public HashMap<PartOrder, Person> getPartOrders() {
		return partOrders;
	}

	public void setPartOrders(HashMap<PartOrder, Person> partOrders) {
		this.partOrders = partOrders;
	}

	public HashSet<Person> getPersons() {
		return persons;
	}

	public void setPersons(HashSet<Person> persons) {
		this.persons = persons;
	}

	public HashMap<Service, Integer> getServices() {
		return services;
	}

	public void setServices(HashMap<Service, Integer> services) {
		this.services = services;
	}

	public String getAffilateCode() {
		return affilateCode;
	}

	public Long getAffilateId() {
		return affilateId;
	}

	public void setAffilateId(Long affilateId) {
		this.affilateId = affilateId;
	}

	public void setAffilateCode(String affilateCode) {
		this.affilateCode = affilateCode;
	}
	
}
