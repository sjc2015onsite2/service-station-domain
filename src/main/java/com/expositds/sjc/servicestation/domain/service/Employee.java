package com.expositds.sjc.servicestation.domain.service;

import java.util.Map;

import org.joda.time.Interval;
import org.joda.time.Period;

import com.expositds.sjc.servicestation.domain.model.Person;

/**
 * Интерфей предоставляет набор методов для взаимодейстрия финансовых структур с данными сотрудников СТО.
 * 
 * @author Alexey Suslov
 */
public interface Employee {

	/**
	 * Метод возвращает имя заданного сотрудника.
	 * @param employee сотрудник
	 * @return имя
	 */
	public String getName(Person employee);
	
	/**
	 * Метод возвращает заработную плату заданного сотрудника за указанный интервал, с указанным периодом.
	 * @param employee сотрудник
	 * @param interval интервал
	 * @param period период
	 * @return набор интервалов, разбитый по периодам с указанием заработной платы за этот интервал
	 */
	public Map<Interval, Integer> getPersonSalary(Person employee, Interval interval, Period period);

}
