package com.expositds.sjc.servicestation.domain.model;

/**
 * Класс, содержащий информацию о запасной части. Экземпляром данного класса является тип запчасти с названием и артикулом.
 * 
 * @author Alexey Suslov
 */
public class Part {
	
	/**
	 * Идентификатор типа запасной части.
	 */
	private Long partId;
	
	/**
	 * Артикул запчасти.
	 */
	private String article;
	
	/**
	 * Наименование запчасти.
	 */
	private String name;
	
	/**
	 * Создаёт новый тип запчасти.
	 * 
	 * @param article артикул запчасти
	 * @param name наименование запчасти
	 */
	public Part(String article, String name) {
		this.article = article;
		this.name = name;
	}

	public String getArticle() {
		return article;
	}

	public String getName() {
		return name;
	}

	public Long getPartId() {
		return partId;
	}

	public void setPartId(Long partId) {
		this.partId = partId;
	}
	
}
