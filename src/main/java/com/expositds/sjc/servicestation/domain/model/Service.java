package com.expositds.sjc.servicestation.domain.model;

/**
 * Класс, содержащий информацию о работе, услуге. Экземпляром данного класса является тип услуги с названием и артикулом.
 * 
 * @author Alexey Suslov
 */
public class Service {
	
	/**
	 * Идентификатор услуги.
	 */
	private Long serviceId;
	
	/**
	 * Артикул услуги.
	 */
	private String article;
	
	/**
	 * Наименование услуги.
	 */
	private String name; 
	
	/**
	 * @param article артикул услуги
	 * @param name наименование услуги
	 */
	public Service(String article, String name) {
		this.article = article;
		this.name = name;
	}

	public String getArticle() {
		return article;
	}

	public String getName() {
		return name;
	}

	public Long getServiceId() {
		return serviceId;
	}

	public void setServiceId(Long serviceId) {
		this.serviceId = serviceId;
	}
		
}
