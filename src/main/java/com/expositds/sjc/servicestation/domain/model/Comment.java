package com.expositds.sjc.servicestation.domain.model;

import org.joda.time.DateTime;

/**
 * Класс содержит комментарий клиента как о СТО, так и о механике, и необязательно только о них.
 * 
 * @author Alexey Suslov
 */
public class Comment {
	
	/**
	 * Идентификатор комментария.
	 */
	private Long commentId;
	
	/**
	 * Непосредственно отзыв об СТО или механнике.
	 */
	private String comment;
	
	/**
	 * Дата написания отзыва.
	 */
	private DateTime date;
	
	/**
	 * Автор отзыва.
	 */
	private SiteUser author;
	
	/**
	 * Виден ли отзыв посетителям сайта.
	 */
	private boolean visible;
	
	/**
	 * Создаёт коментарий.
	 * 
	 * @param comment комментарий
	 * @param author пользователь, оставивший данный комментарий
	 * @param visible виден ли отзыв посетителям сайта
	 */
	public Comment(String comment, SiteUser author, boolean visible) {
		this.comment = comment;
		this.author = author;
		this.date = new DateTime();
		this.visible = visible;
	}

	public String getComment() {
		return comment;
	}

	public DateTime getDate() {
		return date;
	}

	public SiteUser getAuthor() {
		return author;
	}

	public boolean isVisible() {
		return visible;
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}
	
}
