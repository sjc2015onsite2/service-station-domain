package com.expositds.sjc.servicestation.domain.model;

import java.util.ArrayList;
import java.util.HashMap;

import org.joda.time.DateTime;

/**
 * Класс представляет собой заяву на ремонт.
 * 
 * @author Alexey Suslov
 */
public class Order {
	
	/**
	 * Идентификатор заказа.
	 */
	private Long orderId;
	
	/**
	 * Список запчастей их количство, необходимый, для выполнения заявки.
	 */
	private HashMap<Part, Integer> parts = new HashMap<>();
	
	/**
	 * Список работ, услуг в заказе.
	 */
	private ArrayList<Service> services = new ArrayList<Service>();
	
	/**
	 * Зафиксированный для данной заявки список цен на услуги, работы
	 */
	private HashMap<Service, Integer> orderServicesPriceList = new HashMap<>();
	
	/**
	 * Статус выполнения зааза.
	 */
	private OrderStatus status;
	
	/**
	 * Дата создания заказа.
	 */
	private DateTime createDate;
	
	/**
	 * Дата выполнения заказа.
	 */
	private DateTime completeDate;
	
	/**
	 * Описание проблеммы, с которой клиент обратился в СТО.
	 */
	private String problemDescription;
	
	/**
	 * Сообщение от механика клиенту.
	 */
	private ClientNotification notification;
	
	/**
	 * Идентификатор заявки, который использует клиент и сотрудники.
	 */
	private String clientOrderId;
	
	/**
	 * Контактные данные незарегестрированного пользователя.
	 */
	private String contactData;
	
	
	/**
	 * Создаёт новую заявку.
	 * 
	 * @param problemDescription описание проблеммы, с которой клиент обратился в СТО
	 * @param clientOrderId идентификатор заявки, который использует клиент и сотрудники
	 * @param contactData контактные данные незарегестрированного пользователя
	 */
	public Order(String problemDescription, String clientOrderId, String contactData) {
		this.problemDescription = problemDescription;
		this.clientOrderId = clientOrderId;
		this.contactData = contactData;
		this.status = OrderStatus.NEW;
		this.createDate = new DateTime();
	}
	
	/**
	 * Создаёт новую заявку.
	 * 
	 * @param problemDescription описание проблеммы, с которой клиент обратился в СТО
	 * @param clientOrderId идентификатор заявки, который использует клиент и сотрудники
	 */
	public Order(String problemDescription, String clientOrderId) {
		this(problemDescription, clientOrderId, null);
	}
	
	/**
	 * Создаёт новую заявку.
	 * 
	 * @param problemDescription описание проблеммы, с которой клиент обратился в СТО
	 */
	public Order(String problemDescription) {
		this(problemDescription, null, null);
	}
	
	public HashMap<Part, Integer> getParts() {
		return parts;
	}

	public void setParts(HashMap<Part, Integer> parts) {
		this.parts = parts;
	}

	public ArrayList<Service> getServices() {
		return services;
	}

	public void setServices(ArrayList<Service> services) {
		this.services = services;
	}

	public HashMap<Service, Integer> getOrderPriceList() {
		return orderServicesPriceList;
	}

	public void setOrderPriceList(HashMap<Service, Integer> orderPriceList) {
		this.orderServicesPriceList = orderPriceList;
	}

	public OrderStatus getOrderStatus() {
		return status;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.status = orderStatus;
	}

	public ClientNotification getNotification() {
		return notification;
	}

	public void setNotification(ClientNotification notification) {
		this.notification = notification;
	}

	public String getClientOrderId() {
		return clientOrderId;
	}

	public void setClientOrderId(String clientOrderId) {
		this.clientOrderId = clientOrderId;
	}

	public String getProblemDescription() {
		return problemDescription;
	}

	public DateTime getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(DateTime completeDate) {
		this.completeDate = completeDate;
	}

	public DateTime getCreateDate() {
		return createDate;
	}

	public String getContactData() {
		return contactData;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	
}
