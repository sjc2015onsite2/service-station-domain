package com.expositds.sjc.servicestation.domain.model;

import org.joda.time.DateTime;

/**
 * Класс содержит оценку с указанием даты её выставления.
 * 
 * @author Alexey Suslov
 */

public class Mark {
	
	/**
	 * Идентификатор оценки.
	 */
	private Long markId;
	
	/**
	 * Оценка, оставленная пользователем.
	 */
	private Integer mark;
	
	/**
	 * Дата выставления оценки.
	 */
	private DateTime date;
	
	/**
	 * Автор оценки.
	 */
	private SiteUser author;
	
	/**
	 * Создаёт оценку.
	 * 
	 * @param mark оценка
	 * @param author автор оценки
	 */
	public Mark(Integer mark, SiteUser author) {
		if (mark < 1 ) this.mark = 1;
		else if (mark > 5) this.mark = 5;
		else this.mark = mark;
		this.author = author;
		this.date = new DateTime();
	}

	public Integer getMark() {
		return mark;
	}

	public DateTime getDate() {
		return date;
	}

	public SiteUser getAuthor() {
		return author;
	}

	public Long getMarkId() {
		return markId;
	}

	public void setMarkId(Long markId) {
		this.markId = markId;
	}
	
}
