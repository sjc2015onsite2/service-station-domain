package com.expositds.sjc.servicestation.domain.model;

/**
 * Класс содержит сообщение клиенту в рамках оформленной им заявки.
 * 
 * @author Alexey Suslov
 */
public class ClientNotification {
	
	/**
	 * Идентификатор сообщения клиенту.
	 */
	private Long clientNotificationId;
	
	/**
	 * Непосредственно сообщение клиенту.
	 */
	private String message = new String();
	
	/**
	 * Создаёт сообщение клиенту
	 * 
	 * @param message сообщение клиенту
	 */
	public ClientNotification(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public Long getClientNotification() {
		return clientNotificationId;
	}

	public void setClientNotification(Long clientNotification) {
		this.clientNotificationId = clientNotification;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
