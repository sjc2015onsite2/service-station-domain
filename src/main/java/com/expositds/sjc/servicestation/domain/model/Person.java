package com.expositds.sjc.servicestation.domain.model;

import java.util.TreeMap;

import org.joda.time.Interval;

/**
 * Класс, содержащий информацию о сотруднике.
 * 
 * @author Alexey Suslov
 */
public class Person {
	
	/**
	 * Идентификатор сотрудника.
	 */
	private Long personId;
	
	/**
	 * Имя сотрудника.
	 */
	private String name;
	
	/**
	 * Список интервалов дат и величина заработной платы по дням.
	 */
	private TreeMap<Interval, Integer> salary = new TreeMap<>();
	
	/**
	 * Создаёт нового сотрудника.
	 * 
	 * @param name имя нового сотрудника.
	 */
	public Person(String name) {
		this.name = name;
	}

	public TreeMap<Interval, Integer> getSalary() {
		return salary;
	}

	public void setSalary(TreeMap<Interval, Integer> salary) {
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	
}
