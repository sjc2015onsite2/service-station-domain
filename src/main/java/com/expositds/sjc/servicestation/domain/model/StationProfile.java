package com.expositds.sjc.servicestation.domain.model;

import java.util.HashSet;

/**
 * Профиль СТО на сайте-агрегаторе.
 * 
 * @author Alexey Suslov
 */
public class StationProfile {
	
	/**
	 * Идентификатор профиля станции.
	 */
	private Long stationProfileId;
	
	/**
	 * Список комментариев о СТО.
	 */
	private HashSet<Comment> comments = new HashSet<>();
	
	/**
	 * Список оценок о СТО.
	 */
	private HashSet<Mark> marks = new HashSet<>();

	public HashSet<Comment> getComments() {
		return comments;
	}

	public void setComments(HashSet<Comment> comments) {
		this.comments = comments;
	}

	public HashSet<Mark> getMarks() {
		return marks;
	}

	public void setMarks(HashSet<Mark> marks) {
		this.marks = marks;
	}

	public Long getStationProfileId() {
		return stationProfileId;
	}

	public void setStationProfileId(Long stationProfileId) {
		this.stationProfileId = stationProfileId;
	}
	
}
