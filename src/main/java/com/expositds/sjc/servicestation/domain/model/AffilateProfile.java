package com.expositds.sjc.servicestation.domain.model;

import java.util.TreeMap;
import org.joda.time.Interval;

/**
 * Класс для хранения арендной платы филиала. Арендная плата хранится с расчётом по дням.
 * 
 * @author Alexey Suslov
 */
public class AffilateProfile {
	
	/**
	 * Индентифиатор профиля филиала.
	 */
	private Long affilateProfileId;
	
	/**
	 * Список интервалов дат и величина арендной платы по дням.
	 */
	private TreeMap<Interval, Integer> rent = new TreeMap<>();
	
	public TreeMap<Interval, Integer> getRent() {
		return rent;
	}

	public void setRent(TreeMap<Interval, Integer> rent) {
		this.rent = rent;
	}

	public Long getAffilateProfileId() {
		return affilateProfileId;
	}

	public void setAffilateProfileId(Long affilateProfileId) {
		this.affilateProfileId = affilateProfileId;
	}

	
	
}
