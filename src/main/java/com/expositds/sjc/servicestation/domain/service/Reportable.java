package com.expositds.sjc.servicestation.domain.service;

import java.util.Map;
import java.util.Set;

import org.joda.time.Interval;
import org.joda.time.Period;

import com.expositds.sjc.servicestation.domain.model.Affilate;
import com.expositds.sjc.servicestation.domain.model.Order;
import com.expositds.sjc.servicestation.domain.model.Station;

/**
 * Интерфейс предоставляет набор методов для взаимодействия с моделью данных в области построения отчётов.
 * 
 * @author Alexey Suslov
 */
public interface Reportable {

	/**
	 * Метод расчитывает и возвращает прибыль заданного СТО, сформированныую по выполненым заявкам за указанный интервал, с указанным периодом.
	 * @param seviceStation СТО
	 * @param interval интервал
	 * @param period период
	 * @return набор интервалов, разбитый по периодам с указанием прибыли за этот интервал
	 */
	public Map<Interval, Integer> getServiceStationProfit(Station seviceStation, Interval interval, Period period);

	/**
	 * Метод расчитывает и возвращает прибыль заданного филиала, сформированныую по выполненым заявкам за указанный интервал, с указанным периодом.
	 * @param affilate филиал	
	 * @param interval интервал
	 * @param period период
	 * @return набор интервалов, разбитый по периодам с указанием прибыли за этот интервал
	 */
	public Map<Interval, Integer> getAffilateProfit(Affilate affilate, Interval interval, Period period);

	/**
	 * Метод расчитывает и возвращает затраты на аренду заданного филиала за указанный интервал, с указанным периодом.
	 * @param affilate филиал
	 * @param interval интервал
	 * @param period период
	 * @return набор интервалов, разбитый по периодам с указанием арендной платы за этот интервал
	 */
	public Map<Interval, Integer> getAffilateRent(Affilate affilate, Interval interval, Period period);

	/**
	 * Метод расчитывает и возвращает затраты на аренду заданного СТО за указанный интервал, с указанным периодом.
	 * @param serviceStation СТО
	 * @param interval интервал
	 * @param period период
	 * @return набор интервалов, разбитый по периодам с указанием арендной платы за этот интервал
	 */
	public Map<Interval, Integer> getServiceStationRent(Station serviceStation, Interval interval, Period period);

	/**
	 * Метод расчитывает и возвращает заработную плату механиков заданного СТО за указанный интервал, с указанным периодом.
	 * @param serviceStation СТО
	 * @param interval интервал
	 * @param period период
	 * @return набор интервалов, разбитый по периодам с указанием заработной платы за этот интервал
	 */
	public Map<Interval, Integer> getMechanicsSalary(Station serviceStation, Interval interval, Period period);

	/**
	 * Метод расчитывает и возвращает затраты заданного СТО (заработную плату механиков, других сотрудников, затраты на аренду) за указанный интервал, с указанным периодом.
	 * @param serviceStation СТО
	 * @param interval интервал
	 * @param period период
	 * @return набор интервалов, разбитый по периодам с указанием арендной платы за этот интервал
	 */
	public Map<Interval, Integer> getServiceStationCharges(Station serviceStation, Interval interval, Period period);

	/**
	 * Метод возвращает все заявки заданного СТО.
	 * @param serviceStation СТО
	 * @return список заявок
	 */
	public Set<Order> getServiceStationOrders(Station serviceStation);

}
