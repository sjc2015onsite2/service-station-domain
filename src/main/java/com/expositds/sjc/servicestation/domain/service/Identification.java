package com.expositds.sjc.servicestation.domain.service;

/**
 * Интерфейс предоставляет набор методов для идентификации объектов по ключу.
 * 
 * @author Alexey Suslov
 *
 * @param <T> тип идентифицируемого объекта
 */
public interface Identification<T> {
	
	/**
	 * Метод возвращает объект по его идентификационному номеру.
	 * 
	 * @param id идентификационный номер
	 * @return объект
	 */
	public T getById(Long id);

}
