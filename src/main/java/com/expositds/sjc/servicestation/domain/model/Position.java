package com.expositds.sjc.servicestation.domain.model;

/**
 * Должности сотрудников.
 * 
 * @author Alexey Suslov
 */
public enum Position {
	
	/**
	 * Директор.
	 */
	CEO,

	/**
	 * Бухгалтер.
	 */
	ACCOUNTANT,

	/**
	 * Механик.
	 */
	MECHANIC

}
