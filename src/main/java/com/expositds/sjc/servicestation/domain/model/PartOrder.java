package com.expositds.sjc.servicestation.domain.model;

import java.util.HashMap;

import org.joda.time.DateTime;

/**
 * Класс, содержащий заявку на поставку запчастей.
 * 
 * @author Alexey Suslov
 */
public class PartOrder {
	
	/**
	 * Идентификатор заявки на запчасть.
	 */
	private Long partOrderId;
	
	/**
	 * Список и количество запчастей в заказе.
	 */
	private HashMap<Part, Integer> parts = new HashMap<>();

	/**
	 * Предпологаемая дата поставки запчастей.
	 */
	private DateTime date;
	
	/**
	 * Статус заявки.
	 */
	private PartOrderStatus status;
	
	/**
	 * Идентификатор заявки, который использует клиент и сотрудники.
	 */
	private String clientPartOrderId;
	
	/**
	 * Создаёт новую заявку на поставку запчастей.
	 * 
	 * @param clientPartOrderId идентификатор заявки, который использует клиент и сотрудники
	 */
	public PartOrder(String clientPartOrderId) {
		this.clientPartOrderId = clientPartOrderId;
		this.date = new DateTime();
	}
	
	/**
	 * Создаёт новую заявку на поставку запчастей.
	 */
	public PartOrder() {
		this(null);
		
	}

	public HashMap<Part, Integer> getParts() {
		return parts;
	}

	public void setParts(HashMap<Part, Integer> parts) {
		this.parts = parts;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public PartOrderStatus getStatus() {
		return status;
	}

	public void setStatus(PartOrderStatus status) {
		this.status = status;
	}

	public String getClientPartOrderId() {
		return clientPartOrderId;
	}

	public void setClientPartOrderId(String clientPartOrderId) {
		this.clientPartOrderId = clientPartOrderId;
	}

	public Long getPartOrderId() {
		return partOrderId;
	}

	public void setPartOrderId(Long partOrderId) {
		this.partOrderId = partOrderId;
	}
	
}
