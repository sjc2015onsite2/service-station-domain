package com.expositds.sjc.servicestation.domain.model;

import java.util.HashMap;
 
/**
 * Класс содержит информацию о пользователе сайта.
 * 
 * @author Alexey Suslov
 */
public class SiteUser {
	
	/**
	 * Идентификатор пользователя сайта.
	 */
	private Long siteUserId;
	
	/**
	 * Список заявок пользователя сайта.
	 */
	private HashMap<Order, Station> orders = new HashMap<>();

	/**
	 * Имя пользователя
	 */
	private String name;

	/**
	 * Создаёт нового пользователя.
	 * 
	 * @param name имя
	 */
	public SiteUser(String name) {
		this.name = name;
	}

	public HashMap<Order, Station> getOrders() {
		return orders;
	}

	public void setOrders(HashMap<Order, Station> orders) {
		this.orders = orders;
	}

	public String getName() {
		return name;
	}

	public Long getSiteUserId() {
		return siteUserId;
	}

	public void setSiteUserId(Long siteUserId) {
		this.siteUserId = siteUserId;
	}
	
}
