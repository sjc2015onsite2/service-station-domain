package com.expositds.sjc.servicestation.domain.service;

import org.joda.time.Interval;

import com.expositds.sjc.servicestation.domain.model.Affilate;
import com.expositds.sjc.servicestation.domain.model.Person;

/**
 * Интерфейс предоставляет набор методов для взаимодействия с моделью данных в области построения отчётов и ввода первичной информации.
 * 
 * @author Alexey Suslov
 */
public interface Financial extends Reportable {
	
	/**
	 * Устанавливает арендную плату заданому филиалу в заданный интервал. Устанавливается дневная арендная плата.
	 * @param affilate филиал
	 * @param interval интервал
	 * @param rentValue величина арендной платы в день
	 */
	public void setAffilateRent(Affilate affilate, Interval interval, int rentValue);

	/**
	 * Устанавливает заработную плату заданому сотруднику в заданный интервал. Устанавливается дневная заработная плата.
	 * @param employee сотрудник
	 * @param interval интервал
	 * @param salaryValue величина зароботной платы в день
	 */
	public void setSalary(Person employee, Interval interval, int salaryValue);

}
